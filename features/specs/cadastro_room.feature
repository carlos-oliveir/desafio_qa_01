#language:pt
Funcionalidade: Cadastro de room(quarto)
    Para que o sistema possa oferecer varios tipos de quartos devo realizar o cadastro
    Sendo um Administrador do sistema


 @login 
 Cenario: Adicionar quarto

    Dado dado que eu estou na tela de cadastro de quartos
    E preencho os dados gerais 
    E escolho os opcionais do quarto
    Quando finalizo o cadastro
    Então devo ver a mensagem "CHANGES SAVED!"