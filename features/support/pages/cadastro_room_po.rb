# frozen_string_literal: true

class CadastroRoom
  include Capybara::DSL
  require 'faker'

  def menu_room
    find_link(href: '#Hotels').click
    click_on 'Rooms'
    find('.add_button').click
  end

  def general
    qtd = Faker::Number.within(1..10)
    select 'Enabled', from: 'roomstatus'
    find('#s2id_autogen1').click
    find('ul', text: 'One-Bedroom Apartment').click
    find('#s2id_autogen3').click
    find('ul', text: 'monks palace').click
    find('input[name=basicprice]').set qtd
    find('input[name=quantity]').set qtd
    find('input[name=minstay]').set qtd
    find('input[name=adults]').set qtd
    find('input[name=children]').set qtd
    find('input[name=extrabeds]').set qtd
    find('input[name=bedcharges]').set qtd
  end

  def opcionais
    find_link(href: '#AMENITIES').click
    first(class: 'iCheck-helper', visible: false).click
  end

  def salvar
    find('#add').click
  end

  def msg
    find('.ui-pnotify-title').text
  end
end
