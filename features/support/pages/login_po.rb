# frozen_string_literal: true

class LoginPage
  include Capybara::DSL
  def acessa
    visit '/supplier'
  end

  def logar(email, senha)
    find('input[name=email]').set email
    find('input[name=password]').set senha
    click_on 'Login'
  end
end
