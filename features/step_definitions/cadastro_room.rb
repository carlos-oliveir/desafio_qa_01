# frozen_string_literal: true

Dado('dado que eu estou na tela de cadastro de quartos') do
  @cadastro_room.menu_room
  sleep 5
end

Dado('preencho os dados gerais') do
  @cadastro_room.general
end

Dado('escolho os opcionais do quarto') do
  @cadastro_room.opcionais
end

Quando('finalizo o cadastro') do
  @cadastro_room.salvar
end

Então('devo ver a mensagem {string}') do |mensagem|
  expect(@cadastro_room.msg).to have_text(mensagem)
end
